/*for_break.c*/
/*Wstęp do programowania*/
/*Program ilustruje użycie instrukcji break*/
#include <stdio.h>

main(){
   printf("Zgadnij moja liczbe!\n");

   int x = 17, y, n;

   for(n = 5; n>0; n--){
      printf("\nLiczba pozostalych prob: %d.\n",n);
      printf("Podaj liczbe calkowita: ");
      scanf("%d",&y);
      if( y == x ){
         printf("Brawo! %d to moja liczba.\n",x);
         break;
      }
   }
   return 0;
}
