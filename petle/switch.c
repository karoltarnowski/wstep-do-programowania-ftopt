/*switch.c*/
/*Wstęp do programowania*/
/*Program ilustruje użycie instrukcji switch*/
#include <stdio.h>

main(){
   char choice;

   printf("Mozesz wybrac jedna opcje.\n");
   printf("1. Pierwsza.\n");
   printf("2. Druga.\n");
   printf("3. Trzecia.\n");
   printf("4. Czwarta. \n");
   printf("Wybierz opcje:\n");
   scanf("%d",&choice);

   switch(choice){
      case(1):
         printf("Wybrales opcje pierwsza.\n");
         break;
      case(2):
         printf("Wybrales opcje druga.\n");
         break;
      case(3):
         printf("Wybrales opcje trzecia.\n");
         break;
      case(4):
         printf("Wybrales opcje czwarta.\n");
         break;
      default:
         printf("Nie ma takiej opcji.\n");
         break;
   }

   return 0;
}











