/*for.c*/
/*Wstęp do programowania*/
/*na podstawie: G. Perry, D. Miller,
Język C Programowanie dla początkujących, Helion, 2014*/

#include <stdio.h>

main(){
   int licznik;

   for(licznik = 1; licznik <= 4; licznik++){
      printf("Licznik ma wartosc %d.\n",licznik);
   }

   for(licznik = 3; licznik >= 1; licznik--){
      printf("Licznik ma wartosc %d.\n",licznik);
   }

   return 0;
}







