/*do_while_scanf.c*/
/*Wstęp do programowania*/
/*Program ilustruje wykorzystanie
  pętli do-while do sprawdzenia
  poprawności podawanych danych.*/

#include <stdio.h>

main(){
   float a;

   do{
      printf("Podaj liczbe dodatnia: ");
      scanf(" %f",&a);
   }while(a <= 0);

   printf("Liczba %g jest dodatnia, brawo!\n",a);

   return 0;
}







