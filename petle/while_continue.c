/*while_continue.c*/
/*Wstęp do programowania*/
/*Program ilustruje użycie instrukcji continue*/
#include <stdio.h>

main(){
   printf("Program oblicza srednia pieciu ocen.\n");

   float ocena, suma = 0;
   int licznik = 0;

   while(licznik < 5){
      printf("Podaj ocene: ");
      scanf("%f",&ocena);
      if(ocena != 2. && \
         ocena != 3. && ocena != 3.5 && \
         ocena != 4. && ocena != 4.5 && \
         ocena != 5. && ocena != 5.5 )
      {
         printf("Podano nieprawidlowa ocene. \
Sprobuj jeszcze raz.\n");
         continue;
      }
      suma += ocena;
      licznik++;
   }

   printf("Srednia ocen to %g.",suma/licznik);

   return 0;
}











