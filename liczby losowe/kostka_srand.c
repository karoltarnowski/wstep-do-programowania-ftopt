/*kostka_srand.c*/
/*Wstęp do programowania*/
/*Program demonstruje
użycie funkcji rand() i srand().*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#define N 6

int main(){
   int kostka;
   char znak;
   time_t t;

   printf("Program symuluje rzuty kostka.\n");
   printf("Aby przerwac dzialanie wprowadz znak x.\n");

   srand(time(&t)); //funkcja srand() zmienia ziarno generatora
   //srand(time(NULL)); - alternatywne wywołanie

   do{
      kostka = rand() % N + 1;
      printf("Na kostce wypadlo %d.\n", kostka);
      scanf(" %c",&znak);
   }while(toupper(znak) != 'X');

   return 0;
}






















