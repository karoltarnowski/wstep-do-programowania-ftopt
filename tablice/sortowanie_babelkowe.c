/*sortowanie_babelkowe.c*/
/*Program demonstruje działanie algorytmu
sortowania bąbelkowego.*/

#include <stdio.h>
#define N 5

int main(){
   int i, j;
   float a[N] = {2,5,9,3,1};
   float t;

   printf("Tablica nieposortowana.\n");
   for(i=0; i<N; i++){
      printf("%g ",a[i]);
   }

   for(i=0; i<N-1; i++){
      for(j=0; j<N-i-1; j++){
         if( a[j] > a[j+1] ){
            t      = a[j];
            a[j]   = a[j+1];
            a[j+1] = t;
         }
      }
   }

   printf("\nTablica posortowana.\n");
   for(i=0; i<N; i++){
      printf("%g ",a[i]);
   }

   return 0;
}
















