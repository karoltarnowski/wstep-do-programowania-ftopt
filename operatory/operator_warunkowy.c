/*operator_warunkowy.c*/
/*Wstęp do programowania*/
/*Program sprawdzający podzielność
liczby całkowitej przez liczby od 2 do 9.
(wykorzystuje operator warunkowy)*/

#include <stdio.h>

main(){
   int liczba;
   printf("Podaj liczbe calkowita: ");
   scanf("%d",&liczba);

   printf("%d %sdzieli sie przez 2.\n",liczba,
          (liczba % 2 == 0) ? "" : "nie ");
   printf("%d %sdzieli sie przez 3.\n",liczba,
          (liczba % 3 == 0) ? "" : "nie ");
   printf("%d %sdzieli sie przez 4.\n",liczba,
          (liczba % 4 == 0) ? "" : "nie ");
   printf("%d %sdzieli sie przez 5.\n",liczba,
          (liczba % 5 == 0) ? "" : "nie ");
   printf("%d %sdzieli sie przez 6.\n",liczba,
          (liczba % 6 == 0) ? "" : "nie ");
   printf("%d %sdzieli sie przez 7.\n",liczba,
          (liczba % 7 == 0) ? "" : "nie ");
   printf("%d %sdzieli sie przez 8.\n",liczba,
          (liczba % 8 == 0) ? "" : "nie ");
   printf("%d %sdzieli sie przez 9.\n",liczba,
          (liczba % 9 == 0) ? "" : "nie ");

   return 0;
}













