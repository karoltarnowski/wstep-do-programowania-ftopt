/*licznik_operator_inkrementacji.c*/
/*Wstęp do programowania*/
/*na podstawie: G. Perry, D. Miller,
Język C Programowanie dla początkujących, Helion, 2014*/

#include <stdio.h>

main(){
   int licznik = 0;

   printf("Licznik ma wartosc %d.\n",++licznik);
   printf("Licznik ma wartosc %d.\n",++licznik);
   printf("Licznik ma wartosc %d.\n",++licznik);
   printf("Licznik ma wartosc %d.\n",++licznik);
   printf("Licznik ma wartosc %d.\n",--licznik);
   printf("Licznik ma wartosc %d.\n",--licznik);
   printf("Licznik ma wartosc %d.\n",--licznik);

   return 0;
}























