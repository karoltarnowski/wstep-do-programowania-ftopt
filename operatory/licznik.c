/*licznik.c*/
/*Wstęp do programowania*/
/*na podstawie: G. Perry, D. Miller,
Język C Programowanie dla początkujących, Helion, 2014*/

#include <stdio.h>

main(){
   int licznik = 0;

   licznik = licznik + 1; //Zwiększenie licznika do 1.
   printf("Licznik ma wartosc %d.\n",licznik);

   licznik = licznik + 1; //Zwiększenie licznika do 2.
   printf("Licznik ma wartosc %d.\n",licznik);

   licznik = licznik + 1; //Zwiększenie licznika do 3.
   printf("Licznik ma wartosc %d.\n",licznik);

   licznik = licznik + 1; //Zwiększenie licznika do 4.
   printf("Licznik ma wartosc %d.\n",licznik);

   licznik = licznik - 1; //Zmniejszenie licznika do 3.
   printf("Licznik ma wartosc %d.\n",licznik);

   licznik = licznik - 1; //Zmniejszenie licznika do 2.
   printf("Licznik ma wartosc %d.\n",licznik);

   licznik = licznik - 1; //Zmniejszenie licznika do 1.
   printf("Licznik ma wartosc %d.\n",licznik);

   return 0;
}























