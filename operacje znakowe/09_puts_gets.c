/*09_puts_gets.c*/
/*Wstep do programowania*/
/*Program demostruje funkcje
puts() i gets()*/

#include <stdio.h>

main(){
   char napis[1000];

   puts("Podaj napis.");

   gets(napis);
   puts(napis);

   return 0;
}
