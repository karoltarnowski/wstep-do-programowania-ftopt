/*rownanie_liniowe.c*/
/*Wstęp do programowania*/
/*Program rozwiązujący równanie liniowe
(bez sprawdzenia poprawności danych)*/
#include <stdio.h>

main(){
   float a, b;

   printf("Program rozwiazuje rownanie a*x + b = 0\n");
   printf("dla podanych wspolczynnikow a oraz b.\n\n");

   //wczytywanie danych
   printf("Podaj a: ");
   scanf("%f",&a);
   printf("Podaj b: ");
   scanf("%f",&b);

   //obliczenie wyniku i wyświetlenie rozwiązania
   printf("Rozwiazaniem jest x = %g\n",-b/a);
   return 0;
}


























