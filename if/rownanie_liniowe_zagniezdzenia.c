/*rownanie_liniowe_zagniezdzenia.c*/
/*Wstęp do programowania*/
/*Program rozwiązujący równanie liniowe*/

#include <stdio.h>

main(){
   float a, b;

   printf("Program rozwiazuje rownanie a*x + b = 0\n");
   printf("dla podanych wspolczynnikow a oraz b.\n\n");

   //wczytywanie danych
   printf("Podaj a: ");
   scanf("%f",&a);
   printf("Podaj b: ");
   scanf("%f",&b);

   //jeśli a == 0
   if(a == 0){
      //jeśli a == 0 i b == 0
      //to równanie jest prawdziwe tożsamościowo
      if(b == 0){
         printf("Rownanie jest prawdziwe\
 dla wszystkich liczb rzeczywistych.\n");
      }
      //jeśli a == 0, a b != 0
      //to równanie jest sprzeczne
      else{
         printf("Rownanie nie ma rozwiazan.\n");
      }
   }
   //jeśli a != 0
   else{
      //obliczenie wyniku i wyświetlenie rozwiązania
      printf("Rozwiazaniem jest x = %g\n",-b/a);
   }
   return 0;
}










