/*rownanie_liniowe_if.c*/
/*Wstęp do programowania*/
/*Program rozwiązujący równanie liniowe
(sprawdza, czy a != 0)*/

#include <stdio.h>

main(){
   float a, b;

   printf("Program rozwiazuje rownanie a*x + b = 0\n");
   printf("dla podanych wspolczynnikow a oraz b.\n\n");

   //wczytywanie danych
   printf("Podaj a: ");
   scanf("%f",&a);
   printf("Podaj b: ");
   scanf("%f",&b);

   //sprawdzenie czy a != 0
   if(a == 0){
      printf("Program nie dziala poprawnie dla a = 0.\n");
      printf("Podaj a: ");
      scanf("%f",&a);
   }

   //obliczenie wyniku i wyświetlenie rozwiązania
   printf("Rozwiazaniem jest x = %g\n",-b/a);
   return 0;
}



















