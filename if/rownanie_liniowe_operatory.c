/*rownanie_liniowe_operatory.c*/
/*Wstęp do programowania*/
/*Program rozwiązujący równanie liniowe
(wykorzystuje operatory logiczne)*/

#include <stdio.h>

main(){
   float a, b;

   printf("Program rozwiazuje rownanie a*x + b = 0\n");
   printf("dla podanych wspolczynnikow a oraz b.\n\n");

   //wczytywanie danych
   printf("Podaj a: ");
   scanf("%f",&a);
   printf("Podaj b: ");
   scanf("%f",&b);

   if(a == 0 && b == 0)//równanie tożsamościowe
      printf("Rownanie jest prawdziwe dla wszystkich liczb rzeczywistych.\n");
   else if(a == 0 && b != 0)//równanie sprzeczne
      printf("Rownanie nie ma rozwiazan.\n");
   else//w przeciwnym przypadku rozwiązanie jednoznaczne
      printf("Rozwiazaniem jest x = %g\n",-b/a);

   return 0;
}


















