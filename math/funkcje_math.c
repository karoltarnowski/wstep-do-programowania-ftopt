/*funkcje_math.c*/
/*Wstep do programowania*/
/*Program demonstruje wybrane
funkcje matematyczne*/

#include <stdio.h>
#include <math.h>

main(){
   double x = 17.5;
   double y = -21.5;
   printf("podloga z %g = %g\n", x, floor(x));
   printf("sufit   z %g = %g\n", x, ceil(x));
   printf("podloga z %g = %g\n", y, floor(y));
   printf("sufit   z %g = %g\n", y, ceil(y));
   printf("\n");
   printf("modul %g = %g\n",y,fabs(y));
   printf("pow(2,3) = %g\n",pow(2,3));
   printf("sqrt(5)  = %g\n",sqrt(5));

   return 0;
}
